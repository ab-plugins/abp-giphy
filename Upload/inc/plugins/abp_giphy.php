<?php
/**
 * Adds a giphy search button in sceditor
 * Copyright 2020 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
define('CN_ABPGIPHY', str_replace('.php', '', basename(__FILE__)));

/**
 * Informations about the plugin
 * @global MyLanguage $lang
 * @return array
 */
function abp_giphy_info() {
    global $lang;
    $lang->load(CN_ABPGIPHY);
    return array(
        'name' => $lang->abp_giphy_name,
        'description' => $lang->abp_giphy_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-giphy',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.2',
        'compatibility' => '18*',
        'codename' => CN_ABPGIPHY
    );
}

/**
 * Installer of the plugin
 * @global DB_Base $db
 * @global MyLanguage $lang
 */
function abp_giphy_install() {
    global $db, $lang;
    $lang->load(CN_ABPGIPHY);
    $settinggroups = [
        'name' => CN_ABPGIPHY,
        'title' => $lang->abp_giphy_setting_title,
        'description' => $lang->abp_giphy_setting_desc,
        'disporder' => 0,
        'isdefault' => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    $settings = [
        [
            'name' => CN_ABPGIPHY . '_key',
            'title' => $lang->abp_giphy_key,
            'description' => $lang->abp_giphy_key_desc,
            'optionscode' => 'text',
            'value' => '',
            'disporder' => 1
        ],
        [
            'name' => CN_ABPGIPHY . '_limit',
            'title' => $lang->abp_giphy_limit,
            'description' => $lang->abp_giphy_limit_desc,
            'optionscode' => 'numeric' . PHP_EOL . 'min=5' . PHP_EOL . 'max=100',
            'value' => 25,
            'disporder' => 2
        ],
        [
            'name' => CN_ABPGIPHY . '_rating',
            'title' => $lang->abp_giphy_rating,
            'description' => $lang->abp_giphy_rating_desc,
            'optionscode' => 'select
g=' . $lang->abp_giphy_rating_g . '
pg=' . $lang->abp_giphy_rating_pg . '
pg-13=' . $lang->abp_giphy_rating_pg13 . '
r=' . $lang->abp_giphy_rating_r,
            'value' => 'g',
            'disporder' => 3
        ]
    ];
    foreach ($settings as $setting) {
        $insert = [
            'name' => $db->escape_string($setting['name']),
            'title' => $db->escape_string($setting['title']),
            'description' => $db->escape_string($setting['description']),
            'optionscode' => $db->escape_string($setting['optionscode']),
            'value' => $db->escape_string($setting['value']),
            'disporder' => $setting['disporder'],
            'gid' => $gid,
        ];
        $db->insert_query('settings', $insert);
    }
    rebuild_settings();
}

/**
 * Uninstaller
 * @global DB_Base $db
 */
function abp_giphy_uninstall() {
    global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPGIPHY . "_%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPGIPHY . "'");
    rebuild_settings();
    abp_giphy_deactivate();
}

/**
 * Checks if plugin is installed
 * @global MyBB $mybb
 * @return boolean
 */
function abp_giphy_is_installed() {
    global $mybb;
    return (array_key_exists(CN_ABPGIPHY . '_key', $mybb->settings));
}

/**
 * Activate the plugin: initialisation of template
 * @see ../adminfunctions_templates.php
 */
function abp_giphy_activate() {
    require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
    find_replace_templatesets('headerinclude',
            '#var modal_zindex = 9999;#',
            'var modal_zindex = 9999;
    <!-- abp_giphy start -->
    abp_giphy_key = "{\$mybb->settings[\'abp_giphy_key\']}";
    abp_giphy_limit = {\$mybb->settings[\'abp_giphy_limit\']};
    abp_giphy_rating = "{\$mybb->settings[\'abp_giphy_rating\']}";
    <!-- abp_giphy end -->');
    find_replace_templatesets('codebuttons',
            '#(<script type="text\/javascript" src="{\$mybb->asset_url}\/jscripts\/bbcodes_sceditor\.js\?ver=\d+"><\/script>)#i',
            "$1" . PHP_EOL .
            '<!-- abp_giphy start -->
<script type="text/javascript" src="{\$mybb->asset_url}/jscripts/abp_giphy.js?ver=1.2"></script>
<!-- abp_giphy end -->'
    );
    find_replace_templatesets('codebuttons', '#' . preg_quote('"undo') . '#i', '"undo,abp_giphy');
    find_replace_templatesets('codebuttons', '#image,#', 'image,abp_giphy,');
    find_replace_templatesets('rinbutquick',
            '#{\$quickquote}#i',
            '<!-- abp_giphy start -->
<script type="text/javascript" src="{\$mybb->asset_url}/jscripts/abp_giphy.js?ver=1.2"></script>
<!-- abp_giphy end -->'.PHP_EOL.'{\$quickquote}');
}

/*
 * Reverts the template
 * @global DB_Base $db
 * @see /inc/adminfunctions_templates.php
 */
function abp_giphy_deactivate() {
    global $db;
    $db->delete_query('templates', "title ='abp_giphy_js'");
    require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
    find_replace_templatesets('headerinclude', '#<!-- abp_giphy start -->.+<!-- abp_giphy end -->#is', '');
    find_replace_templatesets('codebuttons', '#<!-- abp_giphy start -->.+<!-- abp_giphy end -->#is', '');
    find_replace_templatesets('codebuttons', '#,abp_giphy#i', '');
    find_replace_templatesets('rinbutquick', '#<!-- abp_giphy start -->.+<!-- abp_giphy end -->#is', '');
}
