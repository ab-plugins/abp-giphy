<?php
$l['abp_giphy_name'] = 'ABP Giphy';
$l['abp_giphy_desc'] = 'Integrates Giphy search engine in SCEditor';

$l['abp_giphy_setting_title'] = 'Settings for ABP Giphy';
$l['abp_giphy_setting_desc'] = 'Do not forget to get a Giphy key unless it will not work';

$l['abp_giphy_key'] = 'Your Giphy API key';
$l['abp_giphy_key_desc'] = 'If you do not have a key, go to <a href="https://developers.giphy.com/dashboard">giphy</a> and generate one';
$l['abp_giphy_limit'] = 'Search limit';
$l['abp_giphy_limit_desc'] = 'Maximum number of returned pictures';
$l['abp_giphy_rating'] = 'Rating';
$l['abp_giphy_rating_desc'] = 'Filter results by rating';
$l['abp_giphy_rating_g'] = 'General audience';
$l['abp_giphy_rating_pg'] = 'Parental guidance';
$l['abp_giphy_rating_pg13'] = 'Parental guidance strongly advised';
$l['abp_giphy_rating_r'] = 'Restricted';