/**
 * Javascript for abp_giphy plugin
 */

$(function() {
    if (window.abp_giphy) {
        if (window.console && console.warn) {
            console.warn('abp_giphy has already been initialized');
        }
        return;
    }
    window.abp_giphy = {
        delay: 200,
        auto_close: true,
        lang: {
            searching: 'Searching...',
            not_found: 'No results found... <img src="images/smilies/sad.png" style="margin:0;vertical-align:middle;"/>'
        },
        dropDown: $(
            '<div>' +
            '<input type="text" id="abp_giphy_search" placeholder="Search for a GIF..." style="width:90%;">' +
            '<div id="abp_giphy_results" onscroll="abp_giphy.scrolling(this);"><div id="abp_giphy_images"></div></div>' +
            '<div id="abp_giphy_powered"></div>' +
            '</div>'
        )[0],
        init: function () {
            if ($.sceditor && window.toolbar) {
                $.sceditor.command.set('abp_giphy', {
                    tooltip: 'Find a GIF',
                    dropDown: function (editor, caller, callback) {
                        abp_giphy.reset();
                        abp_giphy.editor = editor;
                        abp_giphy.callback = callback;
                        editor.createDropDown(caller, 'abp_giphy', abp_giphy.dropDown);
                        $('#abp_giphy_search', abp_giphy.dropDown)[0].focus();
                    },
                    exec: function (caller) {
                        var editor = this;
                        $.sceditor.command.get('abp_giphy').dropDown(editor, caller, function (gif) {
                            editor.insert('[img]' + gif + '[/img]');
                        });
                    },
                    txtExec: function (caller) {
                        var editor = this;
                        $.sceditor.command.get('abp_giphy').dropDown(editor, caller, function (gif) {
                            editor.insertText('[img]' + gif + '[/img]');
                        });
                    }
                });
                $('head').append(
                    '<style type="text/css">' +
                    '.sceditor-button-abp_giphy div { background-image:url(images/icons/giphy.png) !important; }' +
                    '.sceditor-button-abp_giphy:after, .sceditor-button-abp_giphy:before { content:""; }' +
                    '#abp_giphy_results { width:300px; margin:10px auto; min-height:30px; max-height:300px; overflow-x:hidden; overflow-y:auto; }' +
                    '.abp_giphy_imagelist { line-height:0; column-count:2; column-gap:3px; }' +
                    '.abp_giphy_imagelist img { margin-bottom:3px; cursor:pointer; width:100%; }' +
                    'html #abp_giphy_powered { background:url(images/gpowered.png) no-repeat 50% 50%; height:33px; width:100%; }' +
                    '</style>'
                );
            }
            else if (typeof(CKEDITOR) !== "undefined") {
				var giphy_but = [
					'<span id=\"giphy_toolbar\" class=\"cke_toolbar\" role=\"toolbar\">',
						'<span class=\"cke_toolgroup\" role=\"presentation\">',
							'<a id=\"giphy\" class=\"cke_button cke_button_off giphy\" title=\"Giphy\" tabindex=\"-1\" hidefocus=\"true\" role=\"button\" onblur=\"this.style.cssText = this.style.cssText;\">',
								'<span class=\"cke_button_icon\" style=\"background-image:url(images/icons/giphy.png) !important;\">&nbsp;</span>',
							'</a>',
						'</span>',
					'</span>'
				];
				CKEDITOR.on('instanceReady', function(evt){
					window.abp_giphy.auto_close = false;
					window.abp_giphy.callback = function(gif) {
						MyBBEditor.insertText('[img]' + gif + '[/img]');
						window.abp_giphy.reset();
						$.modal.close();
					};
					var popup_element = document.createElement("div"),
					ck_selector = document.querySelector("div[id^='cke_']");
					$(popup_element).css({'display': 'none','width': 'initial'});;
					$(popup_element).prepend(window.abp_giphy.dropDown);
					ck_selector.after(popup_element);
					$(giphy_but.join('')).appendTo('.cke_toolbox:last');
					($.fn.on || $.fn.live).call($(document), 'click', '#giphy', function (e) {
						$(popup_element).modal({ zIndex: 7 });
					});
				});
                $('head').append(
                    '<style type="text/css">' +
                    '#abp_giphy_results { width:300px; margin:10px auto; min-height:30px; max-height:300px; overflow-x:hidden; overflow-y:auto; }' +
                    '.abp_giphy_imagelist { line-height:0; column-count:2; column-gap:3px; }' +
                    '.abp_giphy_imagelist img { margin-bottom:3px; cursor:pointer; width:100%; }' +
                    'html #abp_giphy_powered { background:url(images/gpowered.png) no-repeat 50% 50%; height:33px; width:100%; }' +
                    '</style>'
                );
            }
        },
        search: function (query) {
            if (abp_giphy.timeout) {
                abp_giphy.abort();
            }
            if (query) {
                abp_giphy.timeout = window.setTimeout(function () {
                    abp_giphy.reset(true, abp_giphy.lang.searching);
                    abp_giphy.query = encodeURIComponent(query);
                    abp_giphy.request = $.get('https://api.giphy.com/v1/gifs/search?q=' + abp_giphy.query + '&limit=' + abp_giphy_limit + '&rating=' + abp_giphy_rating + '&api_key=' + abp_giphy_key, function (data) {
                        abp_giphy.request = null;
                        abp_giphy.offset = data.pagination.offset + abp_giphy_limit;
                        abp_giphy.offset_total = data.pagination.total_count;
                        abp_giphy.reset(true);
                        abp_giphy.addGIF(data);
                    });
                }, abp_giphy.delay);
            } else {
                abp_giphy.reset(true);
            }
        },
        abort: function () {
            if (abp_giphy.timeout) {
                window.clearInterval(abp_giphy.timeout);
                abp_giphy.timeout = null;
            }
            if (abp_giphy.request) {
                abp_giphy.request.abort();
                abp_giphy.request = null;
            }
        },
        addGIF: function (data, loadMore) {
            var gif = data.data,
                i = 0,
                j = gif.length,
                list = $('<div class="abp_giphy_imagelist" />')[0];
            if (j) {
                for (; i < j; i++) {
                    list.appendChild($('<img id="' + gif[i].id + '" src="' + gif[i].images.fixed_width.url + '" />').click(abp_giphy.insert)[0]);
                }
            } else if (!loadMore) {
                abp_giphy.reset(true, abp_giphy.lang.not_found);
            }
            $('#abp_giphy_results', abp_giphy.dropDown).append(list);
        },
        scrolling: function (that) {
            if (that.scrollHeight - that.scrollTop === that.clientHeight) {
                abp_giphy.loadMore();
            }
        },
        loadMore: function () {
            if (abp_giphy.offset < abp_giphy.offset_total) {
                abp_giphy.request = $.get('https://api.giphy.com/v1/gifs/search?q=' + abp_giphy.query + '&offset=' + abp_giphy.offset + '&limit=' + abp_giphy_limit + '&rating=' + abp_giphy_rating + '&api_key=' + abp_giphy_key, function (data) {
                    abp_giphy.request = null;
                    abp_giphy.offset = data.pagination.offset + abp_giphy_limit;
                    abp_giphy.offset_total = data.pagination.total_count;
                    abp_giphy.addGIF(data, true);
                });
            }
        },
        insert: function () {
            abp_giphy.callback('https://media0.giphy.com/media/' + this.id + '/giphy.gif');
            if (abp_giphy.auto_close) {
                abp_giphy.editor.closeDropDown(true);
                abp_giphy.reset();
            }
        },
        reset: function (resultsOnly, newContent) {
            $('#abp_giphy_results', abp_giphy.dropDown).html(newContent ? newContent : '');
            if (!resultsOnly) {
                $('#abp_giphy_search', abp_giphy.dropDown).val('');
            }
        }
    };
    $('#abp_giphy_search', abp_giphy.dropDown)[0].onkeyup = function (e) {
        var key = e.keyCode;
        var ignorekeys = [16, 17, 18, 20, 37, 38, 39, 40];
        if (key && (ignorekeys.indexOf(key) !== -1)) {
            return;
        } else {
            abp_giphy.search(this.value);
        }
    };
    abp_giphy.init();
});