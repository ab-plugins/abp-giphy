# ABP Giphy
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/crazycat)

Integrates Giphy search button in SCEditor, greatly inspired by the development of [vbgamer45](https://www.createmybb.com/)

![Screenshot](Documents/abp_giphy.png)

## Note
You **need a Giphy API key** to have the plugin working.

If you don't have one, go to https://developers.giphy.com/dashboard and generate it, it's free.

If you use [Rin Editor](https://community.mybb.com/mods.php?action=view&pid=657), activate ABP Giphy **after** having activate Rin Editor

## Installation
* Unzip the content of the archive
* Upload everything under Upload/ directory to your forum root
* In ACP, install & activate
* Fill API Key with your key

## Upgrade from 1.0 or 1.1
* **Important** Deactivate previous version
* Unzip the content of the archive
* Upload everything under Upload/ directory to your forum root
* In ACP, reactivate ABP Giphy

## Settings
* API key (mandatory)
* Limit : number of pictures returned (default: 25)
* Rating : rating filter

## Demonstration
![Demonstration](Documents/ABP_Giphy.webm)

# Thanks
* [vbgamer45](https://www.createmybb.com/) for the initial development
* [martec](https://community.mybb.com/user-49058.html) for Rin Editor and the adaptation
